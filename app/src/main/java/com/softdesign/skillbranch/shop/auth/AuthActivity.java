package com.softdesign.skillbranch.shop.auth;

import android.os.Bundle;

import com.softdesign.skillbranch.shop.R;
import com.softdesign.skillbranch.shop.core.BaseActivity;


public class AuthActivity extends BaseActivity {

    AuthFragment mAuthFragment;

    //region ========================Life cicle===============================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        mAuthFragment = new AuthFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, mAuthFragment)
                .commit();
        if (mAuthFragment.getPresenter().checkUserAuth())
            mAuthFragment.showCatalogScreen();

    }

    @Override
    public void onBackPressed() {
        if (mAuthFragment.getPresenter().isInputStateLogin()) {
            mAuthFragment.getPresenter().setInputStateIdle();
            mAuthFragment.getPresenter().setAlphaAnimation();
        } else
            super.onBackPressed();
    }
    //endregion


}
