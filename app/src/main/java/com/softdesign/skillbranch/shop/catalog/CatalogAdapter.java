package com.softdesign.skillbranch.shop.catalog;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.softdesign.skillbranch.shop.catalog.data.ProductDto;
import com.softdesign.skillbranch.shop.product.ProductFragment;

import java.util.ArrayList;
import java.util.List;



class CatalogAdapter extends FragmentPagerAdapter {

    private List<ProductDto> mProducts;

    CatalogAdapter(FragmentManager fm) {
        super(fm);
        mProducts = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return ProductFragment.newInstance(mProducts.get(position));
    }

    @Override
    public int getCount() {
        return mProducts.size();
    }
    void addItem(ProductDto product){
        mProducts.add(product);
        notifyDataSetChanged();
    }
}
