package com.softdesign.skillbranch.shop.product;

import com.softdesign.skillbranch.shop.catalog.data.ProductDto;
import com.softdesign.skillbranch.shop.core.managers.DataManager;

/**
 * Created by Иван on 29.10.2016.
 */

public class ProductModel {
    DataManager mDataManager =DataManager.getInstance();
    public ProductDto getProductById(int productId){
        // TODO: 29.10.2016 get product from datamaanager
        return mDataManager.getProductById(productId);
    }
    public void updateProduct(ProductDto product){
        mDataManager.updateProduct(product);
    }
}
