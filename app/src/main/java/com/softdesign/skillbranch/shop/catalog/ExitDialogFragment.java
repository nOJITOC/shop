package com.softdesign.skillbranch.shop.catalog;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.softdesign.skillbranch.shop.R;

public class ExitDialogFragment extends DialogFragment {


    public ExitDialogFragment() {
    }

    public static ExitDialogFragment newInstance() {
        ExitDialogFragment frag = new ExitDialogFragment();
        return frag;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.alert_dialog_title)
                .setPositiveButton(R.string.alert_dialog_ok,
                        (dialog, whichButton) -> ((RootActivity)getActivity()).finishActivity()
                )
                .setNegativeButton(R.string.alert_dialog_cancel,null)
                .create();
    }

}
