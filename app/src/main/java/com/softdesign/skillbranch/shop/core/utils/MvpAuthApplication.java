package com.softdesign.skillbranch.shop.core.utils;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class MvpAuthApplication extends Application {
    public static SharedPreferences sSharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }


    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }
}