package com.softdesign.skillbranch.shop.auth;



import com.softdesign.skillbranch.shop.core.layers.view.IView;


public interface IAuthView extends IView {

    void showCatalogScreen();

    IAuthPresenter getPresenter();
    void showAnimation(int alphaIn, int alphaOut);

}
