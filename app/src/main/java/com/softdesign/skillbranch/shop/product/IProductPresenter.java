package com.softdesign.skillbranch.shop.product;

interface IProductPresenter {
    void clickOnPlus();
    void clickOnMinus();
    String showProductCount();
    String showProductPrice();

}
