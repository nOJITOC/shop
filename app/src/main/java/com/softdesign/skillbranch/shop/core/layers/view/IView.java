package com.softdesign.skillbranch.shop.core.layers.view;


public interface IView {
    void showMessage(String message);
    void showError(Throwable t);

    void showLoad();
    void hideLoad();

}
