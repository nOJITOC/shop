package com.softdesign.skillbranch.shop.catalog;

import com.softdesign.skillbranch.shop.catalog.data.ProductDto;
import com.softdesign.skillbranch.shop.core.layers.view.IView;

import java.util.List;


interface ICatalogView extends IView {
    void showAddToCardMessage(ProductDto productDto);
    void showCatalogView(List<ProductDto> productsList);
    void showAuthScreen();

}
