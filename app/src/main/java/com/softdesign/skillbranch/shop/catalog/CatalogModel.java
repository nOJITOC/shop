package com.softdesign.skillbranch.shop.catalog;


import com.softdesign.skillbranch.shop.catalog.data.ProductDto;
import com.softdesign.skillbranch.shop.core.managers.DataManager;

import java.util.List;



class CatalogModel {

    DataManager mDataManager = DataManager.getInstance();
    CatalogModel(){

    }
    List<ProductDto> getProductList(){
        return mDataManager.getProductList();
    }
    boolean isUserAuth(){
        return mDataManager.isAuthUser();
    }
}
