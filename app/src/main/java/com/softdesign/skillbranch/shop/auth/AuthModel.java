package com.softdesign.skillbranch.shop.auth;


import com.softdesign.skillbranch.shop.core.layers.model.IModel;
import com.softdesign.skillbranch.shop.core.managers.DataManager;


class AuthModel  implements IModel {


    private DataManager mDataManager;

    AuthModel() {
        mDataManager = DataManager.getInstance();
    }



    boolean isAuthUser() {
        return mDataManager.getPreferencesManager().isAuthToken();
    }

    void loginUser(String login, String password) {
        // TODO: 20.10.2016 send data to server for auth
        mDataManager.userAuth(login, password);

    }
}
