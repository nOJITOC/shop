package com.softdesign.skillbranch.shop.catalog;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.softdesign.skillbranch.shop.R;
import com.softdesign.skillbranch.shop.account.AccountFragment;
import com.softdesign.skillbranch.shop.auth.AuthFragment;
import com.softdesign.skillbranch.shop.core.BaseActivity;
import com.softdesign.skillbranch.shop.core.layers.view.IView;
import com.softdesign.skillbranch.shop.databinding.ActivityRootBinding;

public class RootActivity extends BaseActivity
        implements IView, NavigationView.OnNavigationItemSelectedListener {

    private String BASKET_TAG = "BASKET_TAG";
    private int BASKET_COUNTER = 0;
    ActivityRootBinding mBinding;
    TextView mBasketCounter;
    FragmentManager mFragmentManager;

    //region =========================Life Cicle==============================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_root);
        mFragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
        }
        if (savedInstanceState != null) {
            BASKET_COUNTER = savedInstanceState.getInt(BASKET_TAG, 0);
        }
        initToolbar();
        initDrawer();
    }

    private void initToolbar() {
        setSupportActionBar(mBinding.toolbar);

    }


    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout,
                mBinding.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mBinding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        mBinding.navView.setNavigationItemSelectedListener(this);
        makeDrawerHeader();

    }


    private void makeDrawerHeader() {
        View view = mBinding.navView.getHeaderView(0);
        final ImageView imageView = (ImageView) view.findViewById(R.id.avatar);
        Glide.with(this)
                .load("http://devintensive.softdesign-apps.ru/storage/user/576564701d37a01c00ee0599/avatar.jpg")// TODO: 31.10.2016  add image
                .asBitmap()
                .placeholder(R.drawable.ic_account_circle_black_24dp)
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(RootActivity.this.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (mFragmentManager.getBackStackEntryCount() == 0) {
            ExitDialogFragment dialogFragment = ExitDialogFragment.newInstance();
            dialogFragment.show(mFragmentManager, "dialog");
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.root_menu, menu);
        MenuItem item = menu.findItem(R.id.action_basket);
        MenuItemCompat.setActionView(item, R.layout.badge_toolbar_item);
        mBasketCounter = getBasketCounterFromMenuItem(item);
        return super.onCreateOptionsMenu(menu);
    }

    private TextView getBasketCounterFromMenuItem(MenuItem item){
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(item);
        return (TextView) notifCount.findViewById(R.id.basket_counter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_basket) {
            return true;// TODO: 01.11.2016 go to basket fragment
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        int id = item.getItemId();
        switch (id) {// TODO: 02.11.2016 replace to abstract fabrica
            case R.id.nav_account:
                fragment = new AccountFragment();
                break;
            case R.id.nav_catalog:
                fragment = new CatalogFragment();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_notifications:
                break;
            case R.id.nav_orders:
                break;
            default:
        }
        if (fragment != null) {
            showFragmentAndAddToBackStack(fragment);
        }
        if (fragment instanceof CatalogFragment) {
            clearBackStack();
        }
        mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showFragmentAndAddToBackStack(Fragment fragment){
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void clearBackStack(){
        mFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(BASKET_TAG, BASKET_COUNTER);
        super.onSaveInstanceState(outState);
    }

    //endregion
    //region =========================IView==============================


    public void updateBasketCount(int count) {
        BASKET_COUNTER = Integer.parseInt(mBasketCounter.getText().toString()) + count;
        mBasketCounter.setText(String.valueOf(BASKET_COUNTER));
    }

    public void finishActivity() {
        finish();
    }

    public void showAuthScreen() {
        AuthFragment login = new AuthFragment();
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, login)
                .addToBackStack(null)
                .commit();
    }

    //endregion
}
