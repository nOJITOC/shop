package com.softdesign.skillbranch.shop.auth;


import android.databinding.Bindable;
import android.support.annotation.Nullable;
import android.util.Patterns;

import com.softdesign.skillbranch.shop.core.layers.presenter.AbstractPresenter;


public class AuthPresenter extends AbstractPresenter<IAuthView> implements IAuthPresenter {
    private static final String EMAIL_ERROR = "Введите существующий e-mail";
    private static final String PASSWORD_ERROR = "Минимум 8 символов (a-Z 0-9)";
    private static AuthPresenter ourInstance = new AuthPresenter();


    //region =======================DataBinding================================
    private String email = "";
    private String password = "";
    private int inputState = AuthInputState.IDLE;

    public String validateEmail(String check) {
        if (isValidEmail(check) || check.isEmpty()) {
            return null;
        } else {
            return EMAIL_ERROR;
        }
    }

    private boolean isValidEmail(String email) {
        return email.matches(Patterns.EMAIL_ADDRESS.pattern());
    }

    public String validatePassword(String check) {
        if (isValidPassword(check) || check.isEmpty()) {
            return null;
        } else {
            return PASSWORD_ERROR;
        }
    }

    private boolean isValidPassword(String password) {
        String pattern = "[\\w]{8,}";
        return password.matches(pattern);
    }


    public boolean isInputStateIdle() {
        return inputState == AuthInputState.IDLE;
    }

    public void setInputStateIdle() {
        inputState = AuthInputState.IDLE;
        notifyChange();
    }

    public boolean isInputStateLogin() {
        return inputState == AuthInputState.LOGIN;
    }

    public void setInputStateLogin() {
        inputState = AuthInputState.LOGIN;
        notifyChange();
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyChange();
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyChange();
    }

    //endregion

    private AuthModel mModel;
    private IAuthView mAuthView;

    private AuthPresenter() {
        mModel = new AuthModel();
    }

    public static AuthPresenter getInstance() {
        return ourInstance;
    }


    @Override
    public void takeView(IAuthView authView) {
        mAuthView = authView;
    }

    @Override
    public void dropView() {
        mAuthView = null;
    }

    @Override
    public void initView() {

    }


    @Nullable
    @Override
    public IAuthView getView() {
        return mAuthView;
    }

    @Override
    public void clickOnLogin() {
        if (getView() != null) {
            if (isInputStateIdle()) {
                setInputStateLogin();
            } else {
//                TODO:  20.10 .2016 auth user
                mModel.loginUser(email, password);
                setInputStateIdle();
                getView().showMessage("request for user auth");
            }
            setAlphaAnimation();
        }

    }

    public void setAlphaAnimation() {
        int alphaOut = 0;
        if (isInputStateIdle()) {
            alphaOut = 0;
        } else if (isInputStateLogin()) {
            alphaOut = 1;
        }
        getView().showAnimation(inputState,alphaOut);
    }

    @Override
    public void clickOnVk() {
        if (getView() != null)
            getView().showMessage("clickOnVk");

    }

    @Override
    public void clickOnFb() {
        if (getView() != null)
            getView().showMessage("clickOnFb");

    }

    @Override
    public void clickOnTwitter() {
        if (getView() != null)
            getView().showMessage("clickOnTwitter");
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView() != null)
            getView().showMessage("Показать каталог");
        // TODO: 29.10.2016 if update complete to catalog screen
        getView().showCatalogScreen();

    }


    @Override
    public boolean checkUserAuth() {
        return mModel.isAuthUser();
    }


}
