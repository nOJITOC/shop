package com.softdesign.skillbranch.shop.product;

import com.softdesign.skillbranch.shop.catalog.data.ProductDto;
import com.softdesign.skillbranch.shop.core.layers.view.IView;

interface IProductView extends IView {
    void showProductView(ProductDto product);

}
