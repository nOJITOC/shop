package com.softdesign.skillbranch.shop.core.layers.presenter;


import android.databinding.BaseObservable;
import android.support.annotation.Nullable;

import com.softdesign.skillbranch.shop.core.layers.view.IView;


public abstract class AbstractPresenter<V extends IView> extends BaseObservable{
    private V mView;

    public void takeView(V view) {
        mView = view;
    }

    public void dropView() {
        mView = null;
    }
    public abstract void initView();

    @Nullable
    public V getView() {
        return mView;
    }



}
