package com.softdesign.skillbranch.shop.product;

import com.softdesign.skillbranch.shop.catalog.data.ProductDto;
import com.softdesign.skillbranch.shop.core.layers.presenter.AbstractPresenter;


public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter {
    private ProductModel mModel;
    private ProductDto mProduct;

    private ProductPresenter(ProductDto product) {
        mModel = new ProductModel();
        mProduct = product;
    }

    //region =========================IProductPresenter==============================
    @Override
    public void clickOnPlus() {
        mProduct.addProduct();
        notifyChange();
    }

    @Override
    public void clickOnMinus() {
        if (mProduct.getCount() > 0)
            mProduct.deleteProduct();
        notifyChange();
    }

    @Override
    public String showProductCount() {
        return String.valueOf(mProduct.getCount());
    }

    @Override
    public String showProductPrice() {
        StringBuilder result = new StringBuilder();
        result.append(mProduct.getPrice()*(mProduct.getCount()>0?mProduct.getCount():1))
                .append(".-");
        return result.toString();
    }

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProduct);
        }

    }


    static ProductPresenter newInstance(ProductDto product) {
        return new ProductPresenter(product);
    }
    //endregion

}
