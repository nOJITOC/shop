package com.softdesign.skillbranch.shop.product;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.softdesign.skillbranch.shop.R;
import com.softdesign.skillbranch.shop.catalog.RootActivity;
import com.softdesign.skillbranch.shop.catalog.data.ProductDto;
import com.softdesign.skillbranch.shop.databinding.FragmentProductBinding;

import java.net.MalformedURLException;
import java.net.URL;


public class ProductFragment extends Fragment implements IProductView,View.OnClickListener {
    private static String PRODUCT = "PRODUCT";
    FragmentProductBinding mBinding;
    private ProductPresenter mPresenter;

    public ProductFragment() {
    }

    public static ProductFragment newInstance(ProductDto product) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PRODUCT, product);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void readBundle(Bundle bundle) {
        ProductDto product = bundle.getParcelable(PRODUCT);
        // TODO: 29.10.2016 presenter init
        mPresenter = ProductPresenterFactory.getInstance(product);
    }

    //region =========================Life Cicle==============================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product, container, false);
        readBundle(getArguments());
        mBinding.setPresenter(mPresenter);
        mPresenter.takeView(this);
        mPresenter.initView();
        View view = mBinding.getRoot();
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }
    //endregion


    //region =========================IProductView==============================

    @Override
    public void showProductView(ProductDto product) {
        mBinding.productNameEt.setText(product.getName());
        mBinding.productDescriptionEt.setText(product.getDescription());
        mBinding.productCountTv.setText(String.valueOf(product.getCount()));
        // TODO: 29.10.2016 Glide
        Glide.with(getRootActivity())
                .load(product.getImageUrl())
                .fitCenter()
                .into(mBinding.productImage);
    }



    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable t) {
        getRootActivity().showError(t);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }


    //endregion
    protected RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;
        }
    }
}
