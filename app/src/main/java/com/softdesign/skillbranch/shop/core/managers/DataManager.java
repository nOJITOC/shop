package com.softdesign.skillbranch.shop.core.managers;


import com.softdesign.skillbranch.shop.catalog.data.ProductDto;

import java.util.ArrayList;
import java.util.List;

public class DataManager {

    private static final String TAG = "DataManager";
    private static DataManager INSTANCE = null;
    private PreferencesManager mPreferencesManager;
    private List<ProductDto> mMockProductList;

    // TODO: 30.10.2016 temp for check user, delete after

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;

    }

    private DataManager() {
        generateMockData();
        mPreferencesManager = new PreferencesManager();
    }

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }




    //region =======================Network================================
    public void userAuth(String login, String password) {
        String token = null;
        // TODO: 21.10.2016  add request to server for user token
        mPreferencesManager.saveAuthToken(token);

    }

    //endregion
    public ProductDto getProductById(int productId) {
        // TODO: 29.10.2016 this sample mock data fix me(mb db)
        return mMockProductList.get(productId - 1);
    }

    public void updateProduct(ProductDto product) {
        // TODO: 29.10.2016  update product count or status(something) save in db
        mMockProductList.add(product);
    }
    public List<ProductDto> getProductList(){
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "Синтетическое моторное масло LIQUI MOLY Top Tec", "http://devintensive.softdesign-apps.ru/storage/user/576564701d37a01c00ee0599/photo.jpg", "description 1description 1description 1description 1description 1description 1description 1", 100, 1));
        mMockProductList.add(new ProductDto(2, "LIQUI MOLY Molygen Generation 5W-30 НС-", "http://topsto-crimea.ru/images/detailed/84/301877-0.jpg?t=1457695045", "description 1description 1description 1description 1description 1description 1description 1", 2636, 1));
        mMockProductList.add(new ProductDto(3, "text 3", "image ", "description 1description 1description 1description 1description 1description 1description 1", 300, 1));
        mMockProductList.add(new ProductDto(4, "text 4", "image ", "description 1description 1description 1description 1description 1description 1description 1", 400, 1));
        mMockProductList.add(new ProductDto(5, "text 5", "image ", "description 1description 1description 1description 1description 1description 1description 1", 500, 1));
        mMockProductList.add(new ProductDto(6, "text 6", "image ", "description 1description 1description 1description 1description 1description 1description 1", 600, 1));
        mMockProductList.add(new ProductDto(7, "text 7", "image ", "description 1description 1description 1description 1description 1description 1description 1", 700, 1));
        mMockProductList.add(new ProductDto(8, "text 8", "image ", "description 1description 1description 1description 1description 1description 1description 1", 800, 1));
        mMockProductList.add(new ProductDto(9, "text 9", "image ", "description 1description 1description 1description 1description 1description 1description 1", 900, 1));
        mMockProductList.add(new ProductDto(10, "text 10", "image ", "description 1description 1description 1description 1description 1description 1description 1", 1000, 1));
        mMockProductList.add(new ProductDto(11, "text 11", "image ", "description 1description 1description 1description 1description 1description 1description 1", 1100, 1));
        mMockProductList.add(new ProductDto(12, "text 12", "image ", "description 1description 1description 1description 1description 1description 1description 1", 1200, 1));
    }

    private boolean cond;
    public boolean isAuthUser() {
        // TODO: 29.10.2016 check user auth token from sp
        cond=!cond;
        return cond;
    }
}

