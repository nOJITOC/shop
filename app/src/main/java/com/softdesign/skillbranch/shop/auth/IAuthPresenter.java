package com.softdesign.skillbranch.shop.auth;

public interface IAuthPresenter{

    void clickOnLogin();
    void clickOnVk();
    void clickOnFb();
    void clickOnTwitter();
    void clickOnShowCatalog();
    boolean isInputStateIdle();
    void setAlphaAnimation();
    boolean isInputStateLogin();
    void setInputStateIdle();
    boolean checkUserAuth();



}
