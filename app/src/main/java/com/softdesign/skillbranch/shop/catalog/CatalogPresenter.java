package com.softdesign.skillbranch.shop.catalog;

import com.softdesign.skillbranch.shop.catalog.data.ProductDto;
import com.softdesign.skillbranch.shop.core.layers.presenter.AbstractPresenter;

import java.util.List;


public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter {
    private static CatalogPresenter ourInstance = new CatalogPresenter();
    private CatalogModel mModel;
    private List<ProductDto> mProducts;

    public static CatalogPresenter getInstance() {
        return ourInstance;
    }

    private CatalogPresenter() {
        mModel = new CatalogModel();

    }

    @Override
    public void initView() {
        if (mProducts == null) {
            mProducts = mModel.getProductList();
        }

        if (getView() != null) {
            getView().showCatalogView(mProducts);
        }
    }

    @Override
    public void clickOnBuyButton(int position) {
        if (getView() != null) {
            if (checkUserAuth()) {
                // TODO: 01.11.2016 положить продукт в корзину
                getView().showAddToCardMessage(mProducts.get(position));
            } else {
                getView().showAuthScreen();
            }
        }

    }

    @Override
    public boolean checkUserAuth() {
        return mModel.isUserAuth();
    }

}