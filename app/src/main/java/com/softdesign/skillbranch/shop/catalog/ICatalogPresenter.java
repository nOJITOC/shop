package com.softdesign.skillbranch.shop.catalog;

interface ICatalogPresenter {
    void clickOnBuyButton(int position);
    boolean checkUserAuth();
}
