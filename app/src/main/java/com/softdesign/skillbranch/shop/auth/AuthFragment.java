package com.softdesign.skillbranch.shop.auth;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.softdesign.skillbranch.shop.R;
import com.softdesign.skillbranch.shop.catalog.RootActivity;
import com.softdesign.skillbranch.shop.core.BaseActivity;
import com.softdesign.skillbranch.shop.databinding.FragmentAuthBinding;

public class AuthFragment extends Fragment implements IAuthView {
    private static final long ANIMATION_DURATION = 1000;
    FragmentAuthBinding mBinding;
    AuthPresenter mPresenter;

    //region =========================Life Cicle==============================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    public AuthFragment() {
        mPresenter = AuthPresenter.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_auth, container, false);
        mBinding.setPresenter(mPresenter);
        mPresenter.takeView(this);
        mPresenter.initView();
        return mBinding.getRoot();
    }

    @Override
    public void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }
    //endregion


    //region =========================IAuthView==============================
    @Override
    public void showAnimation(int alphaIn, int alphaOut){
        setAlphaAnimation(mBinding.authCard,alphaIn,alphaOut);
        setAlphaAnimation(mBinding.showCatalogBtn,alphaOut,alphaIn);
    }

    private void setAlphaAnimation(View view, int alphaIn, int alphaOut) {
        Animation animation = new AlphaAnimation(alphaIn, alphaOut);
        animation.setDuration(ANIMATION_DURATION);
        view.clearAnimation();
        view.startAnimation(animation);


    }

    @Override
    public void showCatalogScreen() {
        Intent intent = new Intent(getActivity(), RootActivity.class);
        startActivity(intent);
        getRootActivity().finish();
    }

    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable t) {
        getRootActivity().showError(t);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }

    //endregion
    private BaseActivity getRootActivity() {
        return (BaseActivity) getActivity();
    }
}
