package com.softdesign.skillbranch.shop.core.utils;


import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

public class BundingUtils {
    private static final long DURATION_ANIMATION = 1000;
    private static String FONTS = "fonts/";


    @BindingAdapter("errorText")
    public static void bindError(TextInputLayout textInputLayout, final String error) {
        textInputLayout.setErrorEnabled(error != null);
        textInputLayout.setError(error);
    }
    @BindingAdapter({"font"})
    public static void setFont(TextView textView, String fontName) {

        textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), FONTS +fontName));
    }

    @BindingAdapter("animationAlpha")
    public static void animation(View view, boolean isVisible) {
        float alphaIn, alphaOut;
        if (isVisible) {
            alphaIn = 0f;
            alphaOut = 1.0f;
        } else {
            alphaIn = 1.0f;
            alphaOut = 0f;
        }
        Animation animation = new AlphaAnimation(alphaIn, alphaOut);
        animation.setDuration(DURATION_ANIMATION);
        view.clearAnimation();
        view.setAnimation(animation);


    }

    @BindingConversion
    public static int convertConditionToVisibility(final boolean condition) {
        return condition ? View.VISIBLE : View.GONE;
    }
}
