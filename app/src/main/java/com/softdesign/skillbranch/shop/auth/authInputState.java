package com.softdesign.skillbranch.shop.auth;

import android.support.annotation.IntDef;

@IntDef({
        AuthInputState.LOGIN,
        AuthInputState.IDLE
})

public @interface AuthInputState {
    int LOGIN = 0;
    int IDLE = 1;
}
