package com.softdesign.skillbranch.shop.catalog.data;

import android.os.Parcel;
import android.os.Parcelable;


public class ProductDto implements Parcelable{
    private int id;
    private String name;
    private String imageUrl;
    private String description;
    private int price;
    private int count;

    public ProductDto(int id, String name, String imageUrl, String description, int price, int count) {
        this.id = id;
        this.name = name;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.count = count;
    }

    //region =========================Parcelable==============================
    protected ProductDto(Parcel in) {
        id = in.readInt();
        name = in.readString();
        imageUrl = in.readString();
        description = in.readString();
        price = in.readInt();
        count = in.readInt();
    }

    public static final Creator<ProductDto> CREATOR = new Creator<ProductDto>() {
        @Override
        public ProductDto createFromParcel(Parcel in) {
            return new ProductDto(in);
        }

        @Override
        public ProductDto[] newArray(int size) {
            return new ProductDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(imageUrl);
        parcel.writeString(description);
        parcel.writeInt(price);
        parcel.writeInt(count);
    }

    //endregion
    //region =========================Getters==============================

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    public void deleteProduct() {
            count--;
    }

    public void addProduct() {
        count++;
    }

    //endregion

}
