package com.softdesign.skillbranch.shop.catalog;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.skillbranch.shop.R;
import com.softdesign.skillbranch.shop.catalog.data.ProductDto;
import com.softdesign.skillbranch.shop.databinding.FragmentCatalogBinding;

import java.util.List;


public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {
    FragmentCatalogBinding mBinding;
    private CatalogPresenter mPresenter;

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.add_to_card_btn)
        {
            mPresenter.clickOnBuyButton(mBinding.productPager.getCurrentItem());
        }

    }

    public CatalogFragment() {

    }

    //region =========================Life Cicle==============================
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_catalog,  container,false);
        mPresenter = CatalogPresenter.getInstance();
        mBinding.setPresenter(mPresenter);
        mPresenter.takeView(this);
        mPresenter.initView();
        mBinding.addToCardBtn.setOnClickListener(this);
        mBinding.pagerIndicator.setViewPager(mBinding.productPager);
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    //endregion

    //region =========================ICatalogView==============================
    @Override
    public void showAddToCardMessage(ProductDto productDto) {
        getRootActivity().updateBasketCount(productDto.getCount());
        showMessage("Товар " + productDto.getName() + " успешно добавлен в корзину.");
    }

    @Override
    public void showCatalogView(List<ProductDto> productsList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDto product : productsList) {
            adapter.addItem(product);
        }
        mBinding.productPager.setAdapter(adapter);

    }

    @Override
    public void showAuthScreen() {
        getRootActivity().showAuthScreen();

    }
    //endregion

    //region =========================IView==============================
    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable t) {
        getRootActivity().showError(t);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }


    //endregion
    protected RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

}
